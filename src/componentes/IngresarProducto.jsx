import React from 'react';
//import MenusView from './MenusView';
import '../css/styles.css';
import { useForm } from "react-hook-form";
import MenusView from './MenusView';
import { ProductoRegistro } from '../hooks/ConexionSw';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import swal from "sweetalert";

function IngresarProducto() {
    const navgacion = useNavigate();
    const msgError = (texto) => swal({

        title: 'Error',
        text: texto,
        icon: 'error',
        button: 'Aceptar',
        timer: 2000

    });

    const msgOk = (texto) => swal({

        title: 'Afirmativo',
        text: texto,

        button: 'Aceptar',
        timer: 2000

    });
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = (data) => {


        var datos = {
            'nombre': data.nombre,
            'descripcion': data.descripcion,
            'cantidad': data.cantidad,
            'marca': data.marca_producto,
            'precio': data.precio,
        };

        const resgistros = ProductoRegistro(datos).then((info) => {
            console.log(info);
            if (info) {
                //  sessionToken(info.token);
                msgOk('Se ha registrado');
                navgacion('/principal');
            }
        }, (error) => {
            //console.log(error.message);    
            msgError(error.message);
        });
    }

    var cantidadDeClaves = Object.keys(errors).length;

    return (
        <div className='row'>
            {errors && cantidadDeClaves > 0 && (
                <p className="alert alert-danger">Faltan datos por llenar</p>)}
            <MenusView />
            <div className='container'>
                <div className="signupFrm">
                    <div className="wrapper">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <h5 className="title">Ingresar productos</h5>
                            <img src="https://elbroker.co/wp-content/uploads/2016/06/icono-productos.png" alt="" 
                            width="100" height="100" />

                            
                                <div className="col">
                                    <div className="form-outline">
                                        <input
                                            type="text"
                                            {...register("nombre", { required: true })}
                                            className="form-control"/>
                                        <label className="form-label">Nombre</label>
                                    </div>
                                </div>
                            
                                <div className="col">
                                    <div className="form-outline">
                                        <input
                                            type="text"
                                            {...register("descripcion", { required: true })}
                                            className="form-control"
                                        />
                                        <label className="form-label">Descripcion</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-outline">
                                        <input
                                            type="text"
                                            {...register("cantidad", { required: true })}
                                            className="form-control"
                                        />
                                        <label className="form-label">Cantidad</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-outline">
                                        <input
                                            type="text"
                                            {...register("marca_producto", { required: true })}
                                            className="form-control"
                                        />
                                        <label className="form-label">Marca</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-outline">
                                        <input
                                            type="text"
                                            {...register("precio", { required: true })}
                                            className="form-control"
                                        />
                                        <label className="form-label">Precio</label>
                                    </div>
                                </div>
                        
                            <button type="submit" className="btn btn-primary btn-block mb-4">
                                Guardar
                            </button>
                                <div>
                                    <Link
                                        to={"/principal"}
                                        className="btn btn-danger btn-block mb-4"
                                        style={{ marginLeft: 5 }}>
                                        Regresar
                                    </Link>
                                </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    );
};
export default IngresarProducto;