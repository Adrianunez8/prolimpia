package com.prolimpia.controladores;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestMethod;

import com.prolimpia.modelo.Persona;

@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})

public interface PersonaRepository extends CrudRepository<Persona, Integer> {

    @Query("SELECT l from persona l WHERE l.external_id = ?1")//lenguaje jpQuery
    Persona findByExternal_id(String external_id);
    /*List<Persona> findByNombreStartsWith(String nombre);
    */
    List<Persona> findByIdentificacion(String identificacion);
  
    
}
