package com.prolimpia.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prolimpia.modelo.Cuenta;

@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })


public interface CuentaRepository extends CrudRepository<Cuenta,Integer>{
    @Query("SELECT c from Cuenta c WHERE c.external_id = ?1")//lenguaje jpQuery
    Cuenta findByExternal_id(String external_id);

    @Query("SELECT c from Cuenta c WHERE c.correo = ?1")//lenguaje jpQuery
    Cuenta findByCorreo(String correo);
}
