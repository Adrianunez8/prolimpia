package com.prolimpia.controladores;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.prolimpia.modelo.Cliente;





import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    @Query("SELECT l from Cliente l WHERE l.external_id = ?1")//lenguaje jpQuery
    Cliente findByExternal_id(String external_id);
    List<Cliente> findByNombreStartsWith(String nombre);
    List<Cliente> findByIdentificacionStartsWith(String identificacion);
  
    
}
