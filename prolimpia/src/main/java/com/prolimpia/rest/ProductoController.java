package com.prolimpia.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prolimpia.controladores.ProductoRepository;
import com.prolimpia.modelo.Producto;
import com.prolimpia.rest.modelo_rest.ProductoWS;
import com.prolimpia.rest.respuesta.RespuestaLista;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;


@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})


@RestController
@RequestMapping(value = "/api/v1")

public class ProductoController {
    @Autowired
    private ProductoRepository ProductoRepository;
  
    
    @GetMapping("/producto")
    public ResponseEntity listar() {
        List<Producto> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        ProductoRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Producto p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("nombre", p.getNombre());
            aux.put("descripcion", p.getDescripcion());
            aux.put("cantidad", p.getCantidad());
            aux.put("external", p.getExternal_id());
            aux.put("marca_producto", p.getMarca_producto());
            aux.put("precio", p.getPrecio());
            aux.put("creado_en", p.getCreateAt());
            aux.put("actualizado_en", p.getUpdateAt());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @PostMapping("/producto/guardar")
    public ResponseEntity guardar(@Valid @RequestBody ProductoWS productows) {
        HashMap mapa = new HashMap<>();
        Producto producto = productows.cargarDatos(null);
        producto.setCreateAt(new Date());
        mapa.put("evento", "Se ha registrado correctamente");
        ProductoRepository.save(producto);
        return RespuestaLista.respuesta(mapa, "OK");

    }
    @GetMapping("/producto/obtener/{external}")
    public ResponseEntity obtener(@PathVariable String external) {
        Producto producto = ProductoRepository.findByExternal_id(external);
        if(producto != null){
            HashMap aux = new HashMap<>();
            aux.put("nombre", producto.getNombre());
            aux.put("descripcion", producto.getDescripcion());
            aux.put("cantidad", producto.getCantidad());
            aux.put("external", producto.getExternal_id());
            aux.put("marca_producto", producto.getMarca_producto());
            aux.put("precio", producto.getPrecio());
            aux.put("creado_en", producto.getCreateAt());
            aux.put("actualizado_en", producto.getUpdateAt());
            return RespuestaLista.respuestaLista(aux);
        }else{
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "no se encontro el objeto deseado");
            return RespuestaLista.respuesta(mapa, "No se encontro el objeto");
        }
    }

   @PostMapping("/producto/editar")
    public ResponseEntity editar (@Valid @RequestBody ProductoWS productows) {
        Producto producto_aux= ProductoRepository.findByExternal_id(productows.getExternal());
        if(producto_aux != null){
            HashMap mapa = new HashMap<>();
            Producto Producto = productows.cargarDatos(producto_aux);
            Producto.setCreateAt(new Date());
            mapa.put("evento", "se ha modificado corectamente");
            ProductoRepository.save(Producto);
            return RespuestaLista.respuesta(mapa, "OK");
            
        }else{
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa,"No se encontro el obleto");
        }

    }
}
