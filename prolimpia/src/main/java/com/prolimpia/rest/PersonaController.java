package com.prolimpia.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.prolimpia.controladores.PersonaRepository;
import com.prolimpia.controladores.RolRepository;
import com.prolimpia.controladores.utiles.Utilidades;
import com.prolimpia.modelo.Cuenta;
import com.prolimpia.modelo.Persona;
import com.prolimpia.modelo.Rol;
import com.prolimpia.rest.modelo_rest.PersonaWS;
import com.prolimpia.rest.respuesta.RespuestaLista;

import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })

public class PersonaController {
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private RolRepository rolRepository;
    @GetMapping("/personas")
    public ResponseEntity listar() {
        List<Persona> lista = new ArrayList<>();
        List  mapa = new ArrayList<>();
        personaRepository.
        findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for(Persona p: lista) {
            cont++;
            HashMap aux = new HashMap<>();            
            aux.put("apellidos", p.getApellidos());
            aux.put("nombres", p.getNombres());
            aux.put("direccion", p.getDireccion());
            aux.put("external", p.getExternal_id());
            aux.put("identificacion", p.getIdentificacion());
            aux.put("tipo", p.getTipo().toString());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }
    @PostMapping("/personas/guardar")
    public ResponseEntity guardar(@Valid @RequestBody PersonaWS personaWS){
        HashMap mapa = new HashMap<>();
        Persona persona = personaWS.cargarObjeto(null);
        Rol rol = rolRepository.findByNombre(personaWS.getTipo_persona().toLowerCase());
        if(rol != null){
            persona.setCreateAt(new Date());
            persona.setRol(rol);
            if (personaWS.getCuenta() != null) {
                Cuenta cuenta = personaWS.getCuenta().cargarObjeto(null);
                cuenta.setClave(Utilidades.clave(personaWS.getCuenta().getClave()));
                cuenta.setEstado(Boolean.TRUE); 
                cuenta.setCreateAt(new Date());
                cuenta.setPersona(persona);
                persona.setCuenta(cuenta);
            }
            personaRepository.save(persona);
            mapa.put("evento", "Se ha registrado correctamente");
            return RespuestaLista.respuesta(mapa, "OK");
        }else {
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el tipo de persona desaeado");
        }
        
    }

    @GetMapping("/personas/obtener/{external}")
    public ResponseEntity obtener(@PathVariable String external_id) {
        Persona p = personaRepository.findByExternal_id(external_id);
        if (p != null) {
            HashMap aux = new HashMap<>();   
   
            aux.put("apellidos", p.getApellidos());
            aux.put("nombres", p.getNombres());
            aux.put("direccion", p.getDireccion());
            aux.put("external", p.getExternal_id());
            aux.put("identificacion", p.getIdentificacion());
            aux.put("tipo", p.getTipo().toString());
            return RespuestaLista.respuestaLista(aux);

        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el onjeto desaeado");
        }
    }
    @PostMapping("/personas/editar")
    public ResponseEntity modificar(@Valid @RequestBody PersonaWS personaws) {
        Persona persona= personaRepository.findByExternal_id(personaws.getExternal());
        if (persona != null) {
            HashMap mapa = new HashMap<>();
            persona = personaws.cargarObjeto(persona);
            mapa.put("evento", "Se ha modificado correctamente");
            personaRepository.save(persona);
            return RespuestaLista.respuesta(mapa, "OK");

        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el objeto desaeado");
        }
    }
    
}
