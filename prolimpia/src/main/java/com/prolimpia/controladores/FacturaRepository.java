package com.prolimpia.controladores;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.prolimpia.modelo.Factura;

public interface FacturaRepository extends CrudRepository<Factura, Integer>
{  
   /*@Query("Select * from Factura f where f.persona.external_id= ?1 ")
    List<Persona> findFacturasById_Persona(String external_id);
    List<Persona> findByPersona(Persona persona);
    
    @Query("Select * from Factura f where f.persona.id= ?1")
     List<Factura> findFacturasById_Persona(String id_persona);
     List<Factura> findByPersona(Persona persona);*/
    

    @Query("Select f from Factura f where f.external_id = ?1")
    Factura findByExternal_id(String external_id);
}
