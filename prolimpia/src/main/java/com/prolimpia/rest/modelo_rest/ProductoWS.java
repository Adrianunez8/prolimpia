package com.prolimpia.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.prolimpia.modelo.Producto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoWS {
    @NotBlank(message = "Campo nombre es requerido")
    @Size(min = 3, max = 100)
    private String nombre;
    @NotBlank(message = "Campo descripcion es requerido")
    @Size(min = 3, max = 200)
    private String descripcion;
    //@NotNull(message = "Campo cantidad es requerido")
    //@Size(min = 3, max = 200)
    private Integer cantidad;

    private String marca_producto;
    
    private String external;
    //@NotNull(message = "Campo precio es requerido")
    //@Size(min = 1, max = 100, message= "el campo precio debe estar incluido en producto")
    private Double precio;

    public Producto cargarDatos(Producto producto){
        
        if(producto == null)
            producto = new Producto();
        producto.setNombre(nombre);
        producto.setDescripcion(descripcion);
        producto.setUpdateAt( new Date());
        producto.setCantidad(cantidad);
        producto.setMarca_producto(marca_producto);
        producto.setExternal_id(UUID.randomUUID().toString()); //cambia el external id cuando se cambia un dato
        producto.setPrecio(precio);
        return producto;
    }
}
