package com.prolimpia.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "producto")
@Getter
@Setter

public class Producto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 30)
    private String nombre;
    @Column(length = 200)
    private String descripcion;
    @Column(length = 5, name = "cantidad", columnDefinition = "integer(5) default 0")
    private Integer cantidad;
    @Column(length = 50, columnDefinition = "varchar(50) default ''")
    private String  marca_producto;
    @Column(columnDefinition = "varchar(60) default uuid()")
    private String external_id;
    @Column(length = 60, columnDefinition = "default 0.0")
    private Double precio;
    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;

    @Override
    public String toString() {
        return nombre;
    }
  
}
