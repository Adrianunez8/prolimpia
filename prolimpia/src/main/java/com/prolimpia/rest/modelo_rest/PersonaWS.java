package com.prolimpia.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.prolimpia.modelo.Persona;
import com.prolimpia.modelo.enums.TipoIdentificacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaWS {
    @NotBlank(message = "Campo apellido es requerido")
    @Size(min = 2, max = 75)
    private String apellidos;
    @NotBlank(message = "Campo nombres es requerido")
    @Size(min = 2, max = 75)
    private String nombres;
    @NotBlank(message = "Campo identificacion es requerido")
    @Size(min = 7, max = 15)
    private String identificacion;
    @NotBlank(message = "Campo identificacion es requerido")
    private String tipo;
    @NotBlank(message = "Campo direccion es requerido")
    @Size(max = 255)
    private String direccion;
    @NotBlank(message = "Campo telefono es requerido")
    @Size(min = 10, max = 15)
    private String telefono;
    private String external;
    private String tipo_persona; 
    private CuentaWS cuenta;

    public Persona cargarObjeto(Persona persona) {
        if (persona == null) {
            persona = new Persona();
        }
        persona.setApellidos(apellidos);
        persona.setDireccion(direccion);
        persona.setExternal_id(UUID.randomUUID().toString());
        persona.setIdentificacion(identificacion);
        persona.setNombres(nombres);
        persona.setTelefono(telefono);
        switch(tipo){
            case "RUC": 
                persona.setTipo(TipoIdentificacion.RUC);
                break;
            case "CEDULA": 
                persona.setTipo(TipoIdentificacion.CEDULA);
                break;
            case "PASAPORTE": 
                persona.setTipo(TipoIdentificacion.PASAPORTE);
                break;
            default:
                break;
        }
        persona.setUpdateAt(new Date());
        return persona;
    }
}
