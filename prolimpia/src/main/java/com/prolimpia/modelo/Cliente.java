package com.prolimpia.modelo;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "cliente")
@Getter
@Setter
public class Cliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    //@NotBlank(message = "autores es requerido")
    //@NotNull(message = "se necesita los autores")
    //@NotEmpty(message = "se necesita los autores")
    //@Size(min = 3, max = 200)
    @Column(length = 200)
    private String nombre;
    //@NotBlank(message = "titulo del libro es requerido")
    //@NotNull(message = "se necesita el titulo del libro")
    //@NotEmpty(message = "se necesita el titulo del libro")
    //@Size(min = 3, max = 250)
    @Column(length = 15)
    private String identificacion;
    @Column(length = 15)
    private String tipo;
    @Column(length = 200)
    private String direccion;
    @Column(length = 15, columnDefinition = "integer(10) default 0")
    private String telefono;


    @CreatedDate
    @Column(name = "create_at", updatable = false, columnDefinition = "datetime default now()")
    private Date createAt;
    @LastModifiedDate
    @Column(name = "update_at", columnDefinition = "datetime default now()")
    private Date updateAt;
    
     // @NotBlank(message = "valor es requerido")
     @Column(columnDefinition = "varchar(36) default uuid()")
     private String external_id;

    @Override
    public String toString() {
        return nombre;
    }
}
