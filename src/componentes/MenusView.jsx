import React from 'react';
import '../css/Bootstrap.css';
import { Link } from 'react-router-dom';
import { CerrarSistema } from '../hooks/ConexionSw';
//const navegacion = useNavigate();
const MenusView = () => {
    const logout = () => {
        CerrarSistema();
        //navegacion('/');
    }
    return (
        <header>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
                <div className="container-fluid">

                    <button
                        className="navbar-toggler"
                        type="button"
                        data-mdb-toggle="collapse"
                        data-mdb-target="#navbarExample01"
                        aria-controls="navbarExample01"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <i className="fas fa-bars"></i>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarExample01">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                            <li className="nav-item active">
                                <Link to="/" className='nav-link' aria-current="page">Prolimpia</Link>
                            </li>
                            <li className="nav-item active">
                                <Link to="/principal" className='nav-link' aria-current="page">Principal</Link>
                            </li>

                            <li className="nav-item">
                                <Link to="/usuarios" className='nav-link'>Usuarios</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/ingresarProducto" className='nav-link'>IngresarProducto</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/listarProducto" className='nav-link'>ListaProducto</Link>
                            </li>

                            <li className="nav-item">
                                <Link to="/" className='nav-link' onClick={logout}>Salir</Link>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    );

};
export default MenusView;