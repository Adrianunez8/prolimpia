package com.prolimpia.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prolimpia.controladores.CuentaRepository;
import com.prolimpia.controladores.utiles.Utilidades;
import com.prolimpia.modelo.Cuenta;
import com.prolimpia.rest.modelo_rest.CuentaWS;
import com.prolimpia.rest.respuesta.RespuestaLista;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.security.access.event.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class CuentaController {
    @Autowired
    private CuentaRepository cuentaRepository;
    
    @PostMapping(value = "/inicio_sesion")
    public ResponseEntity inicioResponseEntity(@Valid @RequestBody CuentaWS cuentaWS) {
        HashMap mapa = new HashMap<>();
        Cuenta cuenta = cuentaRepository.findByCorreo(cuentaWS.getCorreo());
        if (cuenta != null) {
            if (Utilidades.verificar(cuentaWS.getClave(), cuenta.getClave())) {
                mapa.put("token", token(cuenta));
                mapa.put("external", cuenta.getExternal_id());
                mapa.put("correo", cuenta.getCorreo());
 

                return RespuestaLista.respuesta(mapa, "OK");
            } else {
                mapa.put("evento", "Cuenta no encontrada");
                return RespuestaLista.respuesta(mapa, "No se encontr la cuenta con sus credenciales ingresadas");
            }
        } else {
            mapa.put("evento", "Cuenta no encontrada");
            return RespuestaLista.respuesta(mapa, "No se encontr la cuenta con sus credenciales ingresadas");
        }
    }

    private String token(Cuenta cuenta) {
        String secretKey = "Test#";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList(cuenta.getPersona().getRol().getNombre());
        String token = Jwts.builder().setId(cuenta.getExternal_id()).setSubject(cuenta.getCorreo())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 3600000))
                .claim("authorities", grantedAuthorities.stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList()))
                .signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

        return "Bearer " + token;

    }

}
