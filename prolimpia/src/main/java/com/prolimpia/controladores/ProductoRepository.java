package com.prolimpia.controladores; 
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.prolimpia.modelo.Producto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;


@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})

public interface ProductoRepository extends CrudRepository<Producto, Integer> {
  @Query("SELECT p from Producto p WHERE p.external_id = ?1")
    Producto findByExternal_id(String external_id);  //jpql busqueda
    List<Producto>findByNombre(String nombre); 
 
}
