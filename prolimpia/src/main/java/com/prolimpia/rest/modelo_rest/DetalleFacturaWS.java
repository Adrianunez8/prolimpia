package com.prolimpia.rest.modelo_rest;
import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import com.prolimpia.modelo.DetalleFactura;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetalleFacturaWS {
   private ProductoWS productoWs;
    @NotBlank(message = "Cantidad es obligatorio")
    private Integer cantidad;

    public DetalleFactura cargarDetalleFactura(DetalleFactura detalleFactura)
    {
        if(detalleFactura == null) detalleFactura = new DetalleFactura();

        detalleFactura.setCantidad(cantidad);
        detalleFactura.setCreateAt(new Date());
        detalleFactura.setExternal_id(UUID.randomUUID().toString());
        detalleFactura.setPrecioTotal(productoWs.getPrecio()*cantidad);
        detalleFactura.setUpdateAt(new Date());
        return detalleFactura;
    }
}