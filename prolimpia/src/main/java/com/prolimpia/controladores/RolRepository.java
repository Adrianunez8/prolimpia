package com.prolimpia.controladores;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prolimpia.modelo.Rol;

@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})

public interface RolRepository extends CrudRepository<Rol, Integer> {//Integer
    @Query("SELECT r from Rol r WHERE r.nombre = ?1")//lenguaje jpQuery
    Rol findByNombre(String nombre);
    
    //registar, modifcar, obter, listar, (eliminar)
}


//jwt proteccion de rutas 
//crear servicio
//