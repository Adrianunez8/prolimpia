package com.prolimpia.controladores.utiles;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Utilidades {
    public static String clave(String clave) {
        return BCrypt.hashpw(clave, BCrypt.gensalt());
    }

    public static Boolean verificar(String clave, String hash) {
        return BCrypt.checkpw(clave, hash);
    }

    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
