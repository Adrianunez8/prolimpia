package com.prolimpia.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prolimpia.controladores.ClienteRepository;
import com.prolimpia.modelo.Cliente;
import com.prolimpia.rest.modelo_rest.ClienteWS;
import com.prolimpia.rest.respuesta.RespuestaLista;

@RestController
@RequestMapping(value = "/api/v1")
public class ClienteController {
    @Autowired
    private ClienteRepository clienteRepository;
    public static Integer AUTORES = 1;
    public static Integer TITULO = 2;
    public static Integer TODOS = 0;

    @GetMapping("/cliente")
    public ResponseEntity listar() {
        List<Cliente> lista = new ArrayList<>();
        List mapa = new ArrayList<>();
        clienteRepository.findAll().forEach((p) -> lista.add(p));
        Integer cont = 0;
        for (Cliente p : lista) {
            cont++;
            HashMap aux = new HashMap<>();
            aux.put("nombre", p.getNombre());
            aux.put("identificacion", p.getIdentificacion());
            aux.put("tipo", p.getTipo());
            aux.put("telefono", p.getTelefono());
            aux.put("direccion", p.getDireccion());
            aux.put("external", p.getExternal_id());
            aux.put("creado_en", p.getCreateAt());
            aux.put("actualizado_en", p.getUpdateAt());

            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    @PostMapping("/clientes/guardar")
    public ResponseEntity guardar(@Valid @RequestBody ClienteWS clientews) {
        HashMap mapa = new HashMap<>();
        Cliente cliente = clientews.cargarDatos(null);
        clienteRepository.count();
        cliente.setCreateAt(new Date());
        mapa.put("evento", "Se ha registrado correctamente");
        clienteRepository.save(cliente);
        return RespuestaLista.respuesta(mapa, "OK");
    }

    @GetMapping("/clientes/obtener/{external}")
    public ResponseEntity obtener(@PathVariable String external) {
        Cliente p = clienteRepository.findByExternal_id(external);
        if (p != null) {
            HashMap aux = new HashMap<>();
            aux.put("nombre", p.getNombre());
            aux.put("identificacion", p.getIdentificacion());
            aux.put("tipo", p.getTipo());
            aux.put("telefono", p.getTelefono());
            aux.put("direccion", p.getDireccion());
            aux.put("external", p.getExternal_id());
            aux.put("creado_en", p.getCreateAt());
            aux.put("actualizado_en", p.getUpdateAt());
            return RespuestaLista.respuestaLista(aux);

        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el onjeto desaeado");
        }
    }
    
    @PostMapping("/clientes/editar")
    public ResponseEntity modificar(@Valid @RequestBody ClienteWS clientews) {
        Cliente cliente= clienteRepository.findByExternal_id(clientews.getExternal());
        if (cliente != null) {
            HashMap mapa = new HashMap<>();
            cliente = clientews.cargarDatos(cliente);
            mapa.put("evento", "Se ha modificado correctamente");
            clienteRepository.save(cliente);
            return RespuestaLista.respuesta(mapa, "OK");

        } else {
            HashMap mapa = new HashMap<>();
            mapa.put("evento", "Objeto no encontrado");
            return RespuestaLista.respuesta(mapa, "No se encontro el objeto desaeado");
        }
    }

    

    @GetMapping("/clientes/buscar/{tipo}/{texto}")
    public ResponseEntity buscarNombre(@PathVariable Integer tipo, @PathVariable String texto) {
        List<Cliente> lista = new ArrayList<>();
        List mapa = new ArrayList<>();

        if (tipo == 1) {
            clienteRepository.findByNombreStartsWith(texto).forEach((p) -> lista.add(p));
        } else if (tipo == 2) {
            clienteRepository.findByIdentificacionStartsWith(texto).forEach((p) -> lista.add(p));
        } else {
            clienteRepository.findAll().forEach((p) -> lista.add(p));
        }
        for (Cliente p : lista) {
            HashMap aux = new HashMap<>();
            aux.put("nombre", p.getNombre());
            aux.put("identificacion", p.getIdentificacion());
            aux.put("tipo", p.getTipo());
            aux.put("telefono", p.getTelefono());
            aux.put("direccion", p.getDireccion());
            aux.put("external", p.getExternal_id());
            aux.put("creado_en", p.getCreateAt());
            aux.put("actualizado_en", p.getUpdateAt());
            
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);

    } 

}
