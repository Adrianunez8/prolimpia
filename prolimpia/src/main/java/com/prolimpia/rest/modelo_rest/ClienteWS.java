package com.prolimpia.rest.modelo_rest;

import java.util.Date;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.prolimpia.modelo.Cliente;
import com.prolimpia.modelo.Persona;
import com.prolimpia.modelo.enums.TipoIdentificacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteWS {
    
    @NotBlank(message = "Campo nombres es requerido")
    @Size(min = 2, max = 75)
    private String nombre;
    @NotBlank(message = "Campo identificacion es requerido")
    @Size(min = 7, max = 15)
    private String identificacion;
    @NotBlank(message = "Campo identificacion es requerido")
    private String tipo;
    @NotBlank(message = "Campo direccion es requerido")
    @Size(max = 255)
    private String direccion;
    @NotBlank(message = "Campo telefono es requerido")
    
    @Size(min = 10, max = 15)
    private String telefono;
    
    private String external; 

    public Cliente cargarDatos(Cliente cliente) {
        if (cliente == null) {
            cliente = new Cliente();
        }
        cliente.setDireccion(direccion);
        cliente.setExternal_id(UUID.randomUUID().toString());
        cliente.setIdentificacion(identificacion);
        cliente.setNombre(nombre);
        cliente.setTelefono(telefono);
        cliente.setTipo(tipo);
        
        cliente.setUpdateAt(new Date());
        return cliente;
    }
}
