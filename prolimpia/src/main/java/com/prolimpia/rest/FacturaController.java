package com.prolimpia.rest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
import com.prolimpia.controladores.FacturaRepository;
import com.prolimpia.controladores.PersonaRepository;
import com.prolimpia.controladores.*;
import com.prolimpia.modelo.DetalleFactura;
import com.prolimpia.modelo.Factura;
import com.prolimpia.modelo.Persona;
import com.prolimpia.modelo.Producto;
import com.prolimpia.rest.respuesta.RespuestaLista;
import com.prolimpia.rest.respuesta.RespuestaModelo;
import com.prolimpia.rest.modelo_rest.DetalleFacturaWS;
import com.prolimpia.rest.modelo_rest.FacturaWS;

@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD })
public class FacturaController {
  @Autowired
    private FacturaRepository facturaRepository;
@Autowired
    private PersonaRepository personaRepository;
@Autowired
    private ProductoRepository productoRepository;

    @GetMapping("/facturas")
    public ResponseEntity listarFacturas(@PathVariable String external_id)
    {
        List<Factura> facturas = new ArrayList<>();
        List mapa = new ArrayList<>();
        //Añadir metodo para obtener el id de la persona
        //facturaRepository.findFacturasById_Persona(external_id).forEach((p) -> facturas.add(p));        
        HashMap aux = new HashMap<>();
        for(Factura f: facturas)
        {
            aux.put("iva", f.getIVA());
            aux.put("codigo", f.getCodigo());
            aux.put("create_at", f.getCreatedAt());
            aux.put("external_id", f.getExternal_id());
            aux.put("fecha", f.getFecha());
            aux.put("subTotal", f.getSubtotal());
            aux.put("total", f.getTotal());
            aux.put("update_at", f.getUpdatedAt());
            mapa.add(aux);
        }
        return RespuestaLista.respuestaLista(mapa);
    }

    
    @PostMapping(value="/facturas/guardar" )
    public ResponseEntity guardarFactura(@RequestBody FacturaWS facturaWs,@PathVariable String identificacion) 
    {
        HashMap mapa = new HashMap<>();
        Factura factura = facturaWs.cargarFactura(null);
        factura.setCreatedAt(new Date());
        DecimalFormat dc = new DecimalFormat("#########");
        factura.setCodigo(dc.format(facturaRepository.count()+1));
        //Persona persona = personaRepository.findByIdentificacionStartsWith(identificacion);
        Persona persona = (Persona) personaRepository.findByIdentificacion(identificacion); 
        factura.setPersona(persona);
        List<DetalleFactura> df = new ArrayList<>();

        Double subtotal = 0.00;

        for(DetalleFacturaWS dfw: facturaWs.getDetalleFacturasWs())
        {
            DetalleFactura detF = dfw.cargarDetalleFactura(null);
            df.add(detF);
            subtotal += detF.getPrecioTotal();
        }

        factura.setDetalleFacturas(df);
        factura.setSubtotal(subtotal);

        factura.setIVA(subtotal*0.12);
        factura.setTotal(factura.getIVA()+subtotal);
        facturaRepository.save(factura);
        mapa.put("evento","Se ha registrado la factura correctamente");
        return RespuestaLista.respuesta(mapa, "OK");
    }
    

    @GetMapping("facturas/buscar/{external}")
    public ResponseEntity buscarFactura(@PathVariable String external)
    {
        Factura f = facturaRepository.findByExternal_id(external);

        HashMap aux = new HashMap<>();

        aux.put("iva", f.getIVA());
        aux.put("codigo", f.getCodigo());
        aux.put("create_at", f.getCreatedAt());
        aux.put("external_id", f.getExternal_id());
        aux.put("fecha", f.getFecha());
        aux.put("fecha_expiracion", f.getFechaExpiracion());
        aux.put("subTotal", f.getSubtotal());
        aux.put("total", f.getTotal());
        aux.put("update_at", f.getUpdatedAt());
        aux.put("Persona", f.getPersona());
        
        return RespuestaLista.respuesta(aux, "OK");
        
    }

    

}