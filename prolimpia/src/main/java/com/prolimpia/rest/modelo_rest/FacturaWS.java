package com.prolimpia.rest.modelo_rest;

import java.util.Date;
import java.util.List;
import java.util.UUID;


import  com.prolimpia.modelo.Factura;
import lombok.Getter;


@Getter
public class FacturaWS {
    private PersonaWS personaWs;
    private String external_id;
    private List<DetalleFacturaWS> detalleFacturasWs;
   


    public Factura cargarFactura(Factura factura)
    {
        if(factura == null) factura = new Factura();
        
        factura.setExternal_id(UUID.randomUUID().toString());
        factura.setUpdatedAt(new Date());

        factura.setFecha(new Date());
        
        return factura;
    }
}