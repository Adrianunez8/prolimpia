import React from 'react';
import { useState } from "react";
import '../css/Bootstrap.css';
import '../css/prueba.css';
import {Producto} from "../hooks/ConexionSw";
import swal from "sweetalert";
import { Link, Navigate } from "react-router-dom";
import MenusView from './MenusView';

const mensaje = (texto) => swal(
        {
            title: "Error",
            text: texto,
            icon: "error",
            button: "Aceptar",
            timer: 2000
        }
    );
    
    const ListarPedido = () => {
    
        const [info, setInfo] = useState(undefined);
        const [llamada, setLlamada] = useState(false);
        const [show, setShow] = useState(false);
    
        const handleClose = () => setShow(false);    
        const handleShow = () => setShow(true);
        
        if (!llamada) {
            const datos = Producto().then((data) => {
                // llamada = true;
                setLlamada(true);
                console.log(data);

                setInfo(data);
            }, (error) => {
                mensaje(error.message);
            });
        }
    
  return (
    <div>
            <MenusView/>
            <div className='row'>
                <div className='card text-center'></div>
                <div className='col-9'>
                    <div className='card text-center'>
                        <div className='card-body'>
                            <h5 className="card-title alert alert-info">Ver Producto</h5>
                            <div className='card'>
                                <div className='card-body'>
                                    <Link to="/ingresarProducto" className='btn btn-success' onClick={<Navigate to='/ingresarProducto' />}>Agregar mas Productos</Link>
                                </div>

                            </div>
                            <table className='table'>
                                <thead>
                                    <tr>
                                        <th>Nro</th><th>Nombre</th><th>Descripcion</th><th>Cantidad</th><th>Marca</th><th>Precio</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {info && info.data && info.data.map((element, key) => {
                                        return <tr key={key}>
                                            <td>{(key) + 1}</td>
                                            <td>{element.nombre}</td>
                                            <td>{element.descripcion}</td>
                                            <td>{element.cantidad}</td>
                                            <td>{element.marca_producto}</td>
                                            <td>{element.precio}</td>
                                        </tr>
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className='col-3'></div>
            </div>
        </div>
   
    );
};

export default ListarPedido;