-- MariaDB dump 10.19  Distrib 10.6.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: prolimpiafin
-- ------------------------------------------------------
-- Server version	10.6.12-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  `direccion` varchar(200) DEFAULT NULL,
  `external_id` varchar(36) DEFAULT uuid(),
  `identificacion` varchar(15) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `telefono` int(10) DEFAULT 0,
  `tipo` varchar(15) DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuenta`
--

DROP TABLE IF EXISTS `cuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuenta` (
  `id` int(11) NOT NULL,
  `clave` varchar(60) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  `estado` bit(1) DEFAULT NULL,
  `external_id` varchar(36) DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  `id_persona` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s2uct9irk5wtbxn799vx0q6d3` (`id_persona`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuenta`
--

LOCK TABLES `cuenta` WRITE;
/*!40000 ALTER TABLE `cuenta` DISABLE KEYS */;
INSERT INTO `cuenta` VALUES (2,'$2a$10$F/AazLwt/uzaoOomJ0PwfeWAYkZY7k5xMwDfoUWhWlxhTGb0iVCai','Admin@admin.com','2023-02-23 09:05:15','','3dc8d599-d78a-437b-a4b9-bc8b8722b672','2023-02-23 09:05:14',1),(16,'$2a$10$6HFLSw7SkOIOlgEjoc.sWuCayqTvyhcyCir44Qa3T5qf1ZUZdvsGS','omarmaldonado@gmai.com','2023-02-23 15:49:28','','8abed72a-c509-44e9-9b3f-decf02751f44','2023-02-23 15:49:28',15),(18,'$2a$10$V9VXse/wps.6ZhVEhb.hzepCPMGJFRyTTGwb7n3K0EpleZBG33kdS','omarmaldonado@gmai.com','2023-02-23 18:48:38','','2932968c-4aa5-4b2b-8a08-2f23d3bc4c3a','2023-02-23 18:48:38',17),(21,'$2a$10$FjXtFhKmIx.2eUldP8FgQ.lCy8HHF/T.niNMhyb0AhpfrwIvmd4iO','test@test.com','2023-02-24 01:35:41','','681a583b-b73c-4714-a1bf-5c1e5873bc2d','2023-02-24 03:23:28',20),(23,'$2a$10$kwCT4m2mGEvWN2lSwE1tUOeYuQoq3fwz.mzhtH2PpdyyK/WO2SMHS','Admin@Test1.com','2023-02-24 08:41:41','','6484b73a-5e6c-4b35-87cf-009f18bbc6fc','2023-02-24 08:41:41',22);
/*!40000 ALTER TABLE `cuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_factura`
--

DROP TABLE IF EXISTS `detalle_factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_factura` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  `external_id` varchar(50) DEFAULT NULL,
  `precio_total` double DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  `id_factura` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhc4e7swi6aa3e6uom3bwm2x6x` (`id_factura`),
  KEY `FKbtcmmj5awxvxq2gj65kx1v7ly` (`id_producto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_factura`
--

LOCK TABLES `detalle_factura` WRITE;
/*!40000 ALTER TABLE `detalle_factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `iva` double DEFAULT NULL,
  `codigo` varchar(15) DEFAULT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  `external_id` varchar(36) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_expiracion` datetime DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  `id_persona` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKey7npi7iu9rqegpsfg3c6o3ia` (`id_persona`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (3,0,'00001','2023-02-23 09:45:39','0231823e-29b2-4216-8c1a-5172d4553334','2023-02-23 09:45:39',NULL,0,0,'2023-02-23 09:45:39',1),(7,0,'00002','2023-02-23 09:47:27','badcc369-9f36-4964-9a31-241f9fcd6d33','2023-02-23 09:47:27',NULL,0,0,'2023-02-23 09:47:27',1),(11,0,'00003','2023-02-23 10:11:15','77438984-68a8-4ff3-bc5a-15e6e929fa65','2023-02-23 10:11:15',NULL,0,0,'2023-02-23 10:11:15',1);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (25);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  `external_id` varchar(50) DEFAULT NULL,
  `precio_total` double DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  `id_factura` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtxgs8kx26lgcev3un7n520sw` (`id_factura`),
  KEY `FKc1emammk1tjnowrcgjp9ygpjj` (`id_producto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido`
--

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT INTO `pedido` VALUES (4,2,NULL,'ed6a1024-112e-4fbe-9c01-de38774da211',NULL,'2023-02-23 09:45:39',NULL,NULL),(5,2,NULL,'cf416e70-74b7-4b27-b9e1-7004f41e612d',NULL,'2023-02-23 09:45:39',NULL,NULL),(6,2,NULL,'f3bdd005-0165-4a0d-af20-e646069987f3',NULL,'2023-02-23 09:45:39',NULL,NULL),(8,2,NULL,'9c889410-9a14-47ba-b318-0d5433b99a58',NULL,'2023-02-23 09:47:27',NULL,NULL),(9,2,NULL,'9beabb95-785b-4681-8655-c6deb9d00952',NULL,'2023-02-23 09:47:27',NULL,NULL),(10,2,NULL,'147a84c8-df0c-4a77-886f-09e1df064374',NULL,'2023-02-23 09:47:27',NULL,NULL),(12,2,NULL,'54231fd8-3cfc-4054-bc35-cf6cd6f3a082',NULL,'2023-02-23 10:11:15',11,NULL),(13,2,NULL,'e2ed0645-2aa8-43fa-a8b0-ab8460aceedd',NULL,'2023-02-23 10:11:15',11,NULL),(14,2,NULL,'df8be02f-a76b-470c-8d3c-0745d412d487',NULL,'2023-02-23 10:11:15',11,NULL);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `apellidos` varchar(75) DEFAULT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  `direccion` varchar(255) DEFAULT NULL,
  `external_id` varchar(36) DEFAULT NULL,
  `identificacion` varchar(15) DEFAULT NULL,
  `nombres` varchar(75) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  `id_rol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfmpcq2g2sm7s5wb3hrbp0pky` (`id_rol`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Admin','2023-02-23 09:05:14','Test2','d1cd6940-8a3f-43ae-b786-442725564568','88588888889','Test','0000000000','CEDULA','2023-02-23 09:05:14',1),(15,'Chalco','2023-02-23 15:49:28','Zamora','bf5462f7-5744-4c33-ba68-aab1a21df26f','19000000000','Paulina','0000000000','CEDULA','2023-02-23 15:49:28',3),(17,'unl','2023-02-23 18:48:38','Loja','811ce913-171c-4bf0-9f66-65d33fd496c2','000000000','estudiante','0987654321','RUC','2023-02-23 18:48:38',3),(20,'test','2023-02-24 01:35:41','Loja','eb3ac7c6-9fe4-4563-8a1f-6756360b434b','1104973712','test','0000000000','CEDULA','2023-02-24 03:23:28',3),(22,'Test1','2023-02-24 08:41:41','Test1','644cf422-9d9a-4a27-99fe-ad58f9ebb4f7','1104334535','Test1','0000000000','CEDULA','2023-02-24 08:41:41',3);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  `descripcion` varchar(75) DEFAULT NULL,
  `update_at` datetime DEFAULT current_timestamp(),
  `cantidad` int(10) DEFAULT NULL,
  `marca_producto` varchar(75) DEFAULT NULL,
  `external_id` varchar(36) DEFAULT NULL,
  `precio` float(10,5) DEFAULT NULL,
  `create_at` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (19,'clorox','cloro 100gr','2023-02-23 19:31:37',5,NULL,'a4bc2c2b-ca34-4be4-899c-e3bb5671a867',0.50000,'2023-02-23 19:31:37'),(24,'cloro','clorox','2023-02-24 08:49:00',4,NULL,'d436a14b-465f-44d9-acdc-b7e77146687a',4.50000,'2023-02-24 08:49:00');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(75) DEFAULT NULL,
  `nombre` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'administrador','admin'),(2,'administrador','administrador'),(3,'cliente prolimpia','cliente');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-24  9:14:26
